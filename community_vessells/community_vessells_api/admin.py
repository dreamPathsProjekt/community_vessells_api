from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import AbstractUser, User
from .models import CustomUser, Volunteer, Organization, ExpirableProduct, ClothesProduct
from .forms import CustomUserCreationForm


class UserAdmin(BaseUserAdmin):
    add_form = CustomUserCreationForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'first_name', 'last_name', 'api_role', 'password1', 'password2')}
        ),
    )

admin.site.register(CustomUser, UserAdmin)
admin.site.register(Volunteer)
admin.site.register(Organization)
admin.site.register(ExpirableProduct)
admin.site.register(ClothesProduct)
