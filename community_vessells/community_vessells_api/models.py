from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from uuid import uuid4
from datetime import date


class CustomUser(AbstractUser):
    VOLUNTEER = 'VOL'
    ORGANIZATION = 'ORG'

    API_ROLE_CHOICES = (
        (VOLUNTEER, 'Volunteer'),
        (ORGANIZATION, 'Organization')
    )

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.EmailField(unique=True)
    username = models.CharField(blank=True, default='', max_length=150)
    api_role = models.CharField(choices=API_ROLE_CHOICES, max_length=20)

    REQUIRED_FIELDS = ['username', 'api_role']
    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        if self.username == '':
            self.username = self.email
        super(CustomUser, self).save(*args, **kwargs)


class Volunteer(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    avatar = models.URLField(blank=True, default='')

    def __str__(self):
        return self.user.username


class Organization(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE)
    _name = models.CharField(db_column='name', max_length=60, blank=True, default='')
    avatar = models.URLField(blank=True, default='')
    description = models.TextField(blank=True, default='')

    @property
    def name(self):
        if self._name != '':
            return self._name
        return self.user.username

    @name.setter
    def name(self, value):
        self._name = value

    def __str__(self):
        return self.name


@receiver(post_save, sender=CustomUser)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        if instance.api_role == instance.VOLUNTEER:
            Volunteer.objects.get_or_create(user=instance)
        else:
            Organization.objects.get_or_create(user=instance)


@receiver(post_save, sender=CustomUser)
def update_user_profile(sender, instance, **kwargs):
    if instance.api_role == instance.VOLUNTEER:
        instance.volunteer.save()
    else:
        instance.organization.save()


class Product(models.Model):
    FOOD = 'FOOD'
    CLOTHES = 'CLOTHES'
    PHARM = 'PHARM'

    PRODUCT_CHOICES = (
        (FOOD, 'Food Products'),
        (CLOTHES, 'Clothing'),
        (PHARM, 'Medicine')
    )

    title = models.CharField(max_length=250)
    brand = models.CharField(max_length=150, blank=True, default='')
    product_type = models.CharField(choices=PRODUCT_CHOICES, max_length=50)
    count = models.PositiveIntegerField(default=0)
    is_promised = models.BooleanField(default=True)
    date_promised = models.DateField(auto_now_add=True)
    date_stored = models.DateField(blank=True, null=True)
    volunteer_promised = models.ForeignKey('Volunteer', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.title


class ExpirableProduct(Product):
    expire_date = models.DateField()

    @property
    def is_expired(self):
        return date.today() > self.expire_date


class ClothesProduct(Product):
    XXL = 'XXL'
    XL = 'XL'
    L = 'L'
    M = 'M'
    S = 'S'
    XS = 'XS'

    SIZE_CHOICES = (
        (XXL, 'XX Large'),
        (XL, 'Extra Large'),
        (L, 'Large'),
        (M, 'Medium'),
        (S, 'Small'),
        (XS, 'Extra Small')
    )

    MINT = 'MINT'
    NEARMINT = 'NEARMINT'
    VERYGOOD = 'VERYGOOD'
    GOOD = 'GOOD'
    POOR = 'POOR'
    FUBAR = 'FUBAR'

    CONDITION_CHOICES = (
        (MINT, 'Mint'),
        (NEARMINT, 'Near-Mint'),
        (VERYGOOD, 'Very-Good'),
        (GOOD, 'Good'),
        (POOR, 'Poor'),
        (FUBAR, 'Unacceptable')
    )

    size = models.CharField(choices=SIZE_CHOICES, max_length=50)
    item_condition = models.CharField(choices=CONDITION_CHOICES, max_length=50)

    @property
    def is_fubar(self):
        return self.item_condition == self.FUBAR

    def save(self, *args, **kwargs):
        self.product_type = self.CLOTHES
        super(ClothesProduct, self).save(*args, **kwargs)
