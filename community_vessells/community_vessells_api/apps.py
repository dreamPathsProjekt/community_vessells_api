from django.apps import AppConfig


class CommunityVessellsApiConfig(AppConfig):
    name = 'community_vessells_api'
